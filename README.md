# The BioExcel Binder project

This project contains scripts and configuration to setup instances of the
BioExcel Binder portal.

For further details see the [documentation](https://bioexcel-binderhub.readthedocs.io/en/latest/).

# Cluster deployment

We have some script to install:

* [An embassy magnum cluster](cluster_deploy/openstack/sdo_binderhub_prod.sh)
* A GKE cluster (see cluster_deploy/gke/)

## GKE terraform deployment

The deployment will install:

* A zonal GKE cluster using the module [terraform-google-modules/kubernetes-engine/google](https://registry.terraform.io/modules/terraform-google-modules/kubernetes-engine/google/latest)
* A dedicated VPC
* A dedicated service account

Requirements:
* To run the terraform script you need to install and configure [Google Cloud SDK](https://cloud.google.com/sdk).

Quick start:
```
cd cluster_deploy/gke/terraform
terraform init
terraform plan
terraform apply
```

# Helmsman

We are using [helmsman](https://github.com/Praqma/helmsman) to manage the various Bionderhub deployments.

# Quick start

Get a list of all the helmsman applications:

```
make list
```

Select a namespace setting the NAMESPACE env variable:

```
export NAMESPACE=binderhub-noauth-staging
```

Make sure that you have access to the cluster:

```
make cluster_info
```

Initialize the cluster (setup authentication and other basic resources in the namespace, see [the specific doc](cluster_init/README.md)):

```
make cluster_init
```

Simulate the setup the choosen application:

```
make noauth_staging_dry_run
```

Install the application:

```
make noauth_staging
```

# Secrets

The secret.yaml file is encrypted using [Mozilla SOPS](https://github.com/mozilla/sops).

To operate on it you need to be authorized to access the key on our GCP Key
Management System.

The key used to encrypt the files are configured in the file .sops.yaml.

To edit the file:

```
sops secret.yaml
```

To use it with helm:

```
sops exec-file secret.yaml 'helm install binderhub-helm3 jupyterhub/binderhub --version=0.2.0-n563.h72d30fb --namespace=binderhub -f {} -f config.yaml'
```

## Rotate/update keys

1. Edit the file .sops.yaml
2. Run the sub-command "updatekeys":
   ```
   sops updatekeys secret.yaml
   ```

## Using helm-secrets plugin

Install the [helm-secrets plugin](https://github.com/jkroepke/helm-secrets):
```
helm plugin install https://github.com/jkroepke/helm-secrets --version v3.9.1
```

Then you can launch helm prepending "secret". This is a wrapper to helm
commands that is able to detect and decrypt values files.

Eg:

```
helm secrets view secret.yaml
```

```
helm secrets install binderhub-helm3 jupyterhub/binderhub --version=0.2.0-n563.h72d30fb --namespace=binderhub -f secret.yaml -f config.yaml
```
