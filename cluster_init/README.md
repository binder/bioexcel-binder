# Cluster init

Here there are all the configurations required to initialize a brand new
cloud. Eg:

* Setup the access for the deploy agents.
* Setup user accounts.
* ...

After this step - ideally - all the other operation should be done by a
deploy agent athorized to operate on the cluster (eg: helmsman using a
dedicated service account).

# Requirements

You need a kubectl configuration to access the cluster as administrator.

# Quickstart

You just need to provide the kubernetes namespace that will be used to install
binder and the admin service account.

Run:

```
make cluster_init NAMESPACE=<binderhub_namespace>
```

Now the cluster is configured and you can connect to it using the service
account.

# Using make

## Create the kustomization file

In this step you will create/update the kustomization file that will be used
in next steps.

Parameters:

* NAMESPACE (mandatory)

Example:

```
make kustomization.yaml NAMESPACE=binderhub-staging-noauth
```

## Check the changes to be applyed

Show the manifest that will be used:
```
make kustomize
```

Simulate the setup:
```
make dry_run
```

## Deploy on the cluster

To apply the manifest to the remote cluster:
```
make apply
```

## Get the service account token

This is the token to be used by binderhub deploy agents.

This token impersonate the service account: binderhub-setup-admin.

At the moment that service account has cluster-admin privileges. This is
because helmsman/helm requires some access at the cluster scope. This is not
ideal and it will be fixed in further releases (more granular definition of
the service account privileges).

To get the token:
```
make get_token
```
