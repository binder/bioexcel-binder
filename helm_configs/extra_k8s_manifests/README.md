Those manifest files are configuration not managed by helm charts. They are
installed directly on kubernetes using hook scripts (eg: [Helmsman lifecycle hooks](https://github.com/Praqma/helmsman/blob/master/docs/how_to/apps/lifecycle_hooks.md)).

They are used to install secrets of other custom configuration required in the
kubernetes enviroment.
