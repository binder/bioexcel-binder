#!/bin/env bash

openssl req -x509 \
            -nodes \
            -newkey rsa:2048 \
            -keyout tls.key \
            -out tls.crt \
            -subj '/CN=binderhub-ebi-sdo-test' \
            -days 3650 \
            -extensions san \
            -config <( \
echo '[req]'; \
echo 'distinguished_name=req'; \
echo '[san]'; \
echo 'subjectAltName=DNS:binder.tsi.ebi.ac.uk,DNS:bioexcel-binder.tsi.ebi.ac.uk,DNS:*.binderhub.albeus.it,DNS:*.sslip.io')

kubectl kustomize -o binderhub-test-tls-secrets.yaml . 
