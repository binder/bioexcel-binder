Those are SSL certificates for testing enviroment. Do not use in public
instances.

To generate new certificates:
```
make_tls_secrets.sh
```

The script:
* Generates a new self-signed certificate.
* Create the manifest for defining k8s secrets (using kustomize).
