# kube_tag: v1.23.6
# cluster_distro: "fedora-coreos 35"

openstack coe cluster create  sdo-binderhub-prod\
            --cluster-template  ehk-basic-template-v3.1\
            --keypair admin --master-count 3 \
            --master-flavor m1.medium \
            --node-count 4 \
            --flavor 16c32m80d \
            --merge-labels \
            --labels availability_zone=nova,auto_scaling_enabled=false
