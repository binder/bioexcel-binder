.PHONY: list \
        noauth_staging_dry_run \
        noauth_staging \

# Make environment variables visible inside the recipes
.ONESHELL:

# Allow cluster init tasks to be called from the main Makefile
include ./cluster_init/Makefile

list:
	@echo -e "\n$$(tput setaf 7)Helmsman apps (environments) available:"
	@which yq > /dev/null 2>&1 || echo -e "$$(tput setaf 1)\"yq\" missing.\nPlease install it: https://github.com/mikefarah/yq#install$$(tput sgr0)"
	@yq '.apps[] as $$item ireduce ({"<APP>":"<NAMESPACE>"}; .[$$item | key] = ($$item | .namespace) )' < helmsman.yaml 2>/dev/null
	@echo

noauth_staging_dry_run:
	@helmsman --debug \
                  --show-diff \
                  --kubeconfig kube_temp_files/k8s.secret.config \
                  -f helmsman.yaml \
                  --target binderhub-noauth_staging_embassy \
                  --subst-env-values

noauth_staging:
	@helmsman --apply \
                  --kubeconfig kube_temp_files/k8s.secret.config \
                  -f helmsman.yaml \
                  --target binderhub-noauth_staging_embassy \
                  --subst-env-values
