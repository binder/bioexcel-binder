# Kubernetes temporary files 

During the cluster initialization some configuration are created (for details
see cluster_init/README.md). Those are
temporary configuration to let the deployment agent access the cluster:

* Certificate Authority certificate.

* Service account token (secret).

* Cluster configuration to access the cluster as service account (secret).

BEWARE: As you can see some configuration are labelled as "secret". Don't
share them and delete them as soon as you won't need them anymore.
